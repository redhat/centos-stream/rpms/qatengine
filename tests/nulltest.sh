#!/bin/bash

# we cannot test this since there is no machine with QAT_4XXX hardware in Beaker as of now
# Intel promised us to provide OtherQA for qatlib, qatengine and QAT kernel patchsets
echo QATEngine nulltest is PASS
exit 0
